#!/bin/sh

if [ -f config.sh ]
then
	# Loading a config file.
	source config.sh;
else
	echo "\tYou need to setup a config file, take a look to config_sample.sh.";
	exit 2;
fi

network_device=${network_device:-"wi-fi"};
network_interface=${network_interface:-"en0"};
maximum_RTT=${maximum_RTT:-"2"}; # seconds
interval=${interval:-"1"};
errors_limit=${errors_limit:-"3"};

reconnections_counter=0;
errors_counter=0;

echo "\tSetup:";
echo "\t\t- network: $network";
echo "\t\t- password: $password";
echo "\n";

echo_err () {
	echo "$@" 1>&2;
}

getting_current_ssid () {
	result=$(airport -I | grep "\sSSID: " | cut -d: -f2 | tr -d " ");
	if [ "$result" == "" ]
	then
		exit 2;
	fi
	echo "$result";
	exit 0;
}

get_default_gateway () {
	result=$(netstat -rn | grep "^default\s\+" | grep -o "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}");
	if [ "$result" == "" ]
	then
		exit 2;
	fi
	echo "$result";
	exit 0;
}


reconnect () {
	say "Connectivity problems"
	networksetup -setairportpower $network_interface off;
	networksetup -setairportpower $network_interface on;
	# Wait a little till the interface is up again.
	sleep 0.3;
	while true
	do
		echo " Connecting...\c";
		networksetup -setairportnetwork "$network_interface" "$network" $password 2> /dev/null 1> /dev/null
		network_return_code=$?;
		if [ $network_return_code -ne 0 ]
		then
			continue;
		fi
		while true
		do
			current_ssid=$(getting_current_ssid);
			current_ssid_code=$?;
			if [ $current_ssid_code -eq 0 ]
			then
				break;
			fi
			sleep 1;
		done
		if [ "$current_ssid" != "$network" ]
		then
			echo " We are connected to a different network: '$current_ssid' :S\c";
			sleep 1;	
			continue;
		fi
		echo " Getting default gateway \c";
		retries_limit=4;
		getting_gateway_return_code=2;
		while true
		do
			if [ $retries_limit -lt 1 ]
			then
				echo " No gateway found.\c"
				break;
			fi
			retries_limit=$((retries_limit-1));
			gateway=$(get_default_gateway);
			getting_gateway_return_code=$?;
			if [ $getting_gateway_return_code -eq 0 ]
			then
				echo " Gateway: $gateway\c";
				break;
			fi
			sleep 1.5;
		done
		# Retry unless there is a successful ping.
		ping_return_code=-2;
		if [ $getting_gateway_return_code -eq 0 ]
		then
			ping -t $maximum_RTT -c 2 "$gateway" 2>/dev/null 1>/dev/null;
			ping_return_code=$?;
		fi
		if [ $ping_return_code -ne 0 ]
		then
			# echo "\n\tnetwork: $network_return_code";
			# echo "\tping: $ping_return_code";
			echo " [KO] retrying. \c";
			continue;
		else
			echo " [OK] .\c";
			break;
		fi
	done
	reconnections_counter=$((reconnections_counter+1));
	echo "Number: $reconnections_counter. \c";
}

control_c() {
	echo "";
	exit 3;
}

trap control_c SIGINT;

# Checking if we are connected.
networksetup -getairportnetwork "$network_interface" | grep -q "You are not associated with an AirPort network";
if [ $? -eq 0 ]
then
	echo "\tWe aren't connected, lets connect...";
	reconnect;
	continue;
else
	gateway=$(get_default_gateway);
fi
while true
do
	ping -t $maximum_RTT -c 1 "$gateway" 2>/dev/null 1>/dev/null;
	# Checking if the ping was successful.
	if [ $? -ne 0 ]
	then
		errors_counter=$(($errors_counter+1));
		echo "\033[31m!\033[0m\c";
		# echo "\tTimeout or error in the ping: $errors_counter.";
		if [ $errors_counter -ge $errors_limit ]
		then
			errors_counter=0;
			# Reconnecting.
			reconnect;
			continue;
		fi
	else
		echo "\033[32m.\033[0m\c";
		errors_counter=0;
		sleep $interval;
	fi	
done
